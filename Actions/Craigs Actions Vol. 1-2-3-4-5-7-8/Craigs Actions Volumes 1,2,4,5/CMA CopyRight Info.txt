Craig's Actions Copyright Policy:

These materials are all copyright protected. You are granted a one-person right of usage only and are welcome to install them on any computers that you own and are used by you. Companies with multiple computers and with multi-user stations please inquire about specific licenses to address your requirements. You may copy these files for archiving purposes, but you are not allowed to distribute them to others in any form or through any means. 

By opening the folders containing these materials, you are agreeing to be bound by all aspects of Canadian, as well as any and all International Copyright Laws - not just any portions represented here. 

So, not wanting to be heavy or put a downer on your enthusiasm with these new products -  just asking that you please respect the copyright of these materials. 

Thank you!

:-)

Craig
