Source files collected: For all comps

Collected comps:  
	1-Schrit
	1-Schrit 2
	1-Schrit 3
	1-Schrit 4
	1-Schrit 5
	2-Schrit
	
Number of collected files:  6

Size of collected files:  784 KB

Rendering plug-ins:
	Advanced 3D
	
Effects used:  8

Effect:  Brightness & Contrast
	
Effect:  CC Lens
	
Effect:  Color Balance (HLS)
	
Effect:  DE Electrical Arcs
	
Effect:  S_Distort
	
Effect:  S_LensFlare
	
Effect:  Spherize
	
Effect:  T_Lens (2.1)
	

