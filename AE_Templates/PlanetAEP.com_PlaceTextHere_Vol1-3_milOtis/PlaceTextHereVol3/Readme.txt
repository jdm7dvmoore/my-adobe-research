
Thank you for your purchase of pre made after effects projects from PlanetAEP.com.





If you have purchased a collection, click on "PlaceTextHereVol?_MENU.html" within this folder to open a preview page of all titles within the collection. Clicking on the project number will open the project within after effects, ensure to save any other work when prompted to do so, This project will open as a template file so you cannot mofify the original, leaving it in its purchased state for future use.




For single project purchases you have a thumbnail of your selected project.





How to Modify your PlanetAEP After Effects Project




Projects have been supplied as After Effects Template files and can be opened or imported as normal. Template files will open as 'untitled project'. 

As always, ensure you save any currently open work when prompted to do so. 



To import PlanetAEP comps into an existing project. 

Choose: File > Import > File (or press Ctrl+I (Windows) or Command+I (Mac OS)) 



All projects purchased from PlanetAEP are supplied as HD1080 25fps format. To alter these settings, you will need to do the following. 

Change composition settings on 'ALL' comps within your selected PlanetAEP project to whatever size or frame rate you require. 

Choose: Composition > Composition Settings (or press Ctrl+K (Windows) or Command+K (Mac OS)) 


Select the duration required and open out all layers to match this duration. 

If your new selected composition size is much smaller than the supplied 1080, in some circumstances ie. fractals, you will need to move the starting position of your first keyframe to within or just outside the visible area to acheive your desired results. 

For graphics/Titles projects, select comp entitled 'Place Text Here' and edit this caption to your own requirements. 

Reveal Keyframes on layers and move them out to fit your new chosen duration/speed of evolution. 



visit http://www.planetAEP.com for more information and to view regularly added new projects.


The PlanetAEP Team