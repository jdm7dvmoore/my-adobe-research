Planet AEP License Agreement


This is a non-exclusive, non-transferable license agreement between you and PlanetAEP.com.



Please read this agreement carefully before using the product. 

If you choose not to accept this agreement, you must return the
product unused. By using any part of the Contents, you are agreeing to the
following terms:


The Contents may be used, changed, and incorporated in your work. You may
not sell, redistribute, incorporate in a product or give away the contents of this
collection. 

You must accept the Contents �as is� with no express or implied
warranty. The Contents may not be transferred to any third parties through networked
computers. You may not sell, sublicense, loan, give, or transfer any part of the contents to a
third-party, or allow the Contents to be accessed by individuals that are not willing
to comply with this agreement. The Contents are royalty free and may be published, broadcasted and distributed across all media types.