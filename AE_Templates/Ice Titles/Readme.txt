Instructions for basic customization of Ice Titles:

Foreword:

This project file has been created using only the tools provided in After Effects CS3 
Professional so everything is editable and no third party plug-ins are required. Thank you 
for purchasing this project file and I hope it serves you well.

These instructions assume a basic level of After Effects knowledge and cover the basics 
of editing the elements within the template. There are a wide variety of After Effects 
tutorials on the internet that will help you understand the program better should you wish 
to customize this template beyond the basic level. If you come across terms in these 
instructions that you are not familiar with consult your After Effects documentation and 
help files for more information.

Navigation: 

The template has been set up so you can easily find items within it. All the elements 
within the project file are contained in three folders within the Project panel. The 
Comps folder contains all the Comps that make up the sequences within the template. Their
names will tell you where in the sequence of the project they are.
The Assets folder contains the background texture as well as some pre-comped elements
used throughout the project. These elements can be changed by accessing the pre-comps.

The Solids folder contains all the solids, null objects and place holders etc that are 
situated throughout the template. 

Editing Titles:

Find the title text layer you wish to edit in the Timeline panel (Note: the layers are named 
what the text says, e.g. �YOUR TITLE� will be the �YOUR TITLE� text layer). Double 
click the layer name in the Timeline panel and you will see the text highlight in the 
Composition Panel. You can now type whatever text or phrase you would like to 
substitute in. If your text is to large you can scale it up or down using the point slider in 
the Character Panel (accessed via Window > Character). The colour of each of the titles 
can be controlled using the colour picker in the Character panel. The text also has a glow applied
to it so be sure to change that to suit your changes.

Adding your own pictures or video:

There are place holders within the template for you to substitute your own footage or 
images into. Locate the place holder you wish to replace and select the layer. Import 
your own footage or image into After Effects (File > Import) and locate it in the Project 
Panel. With the place holder still selected locate your footage in the Project panel, click 
and drag the cursor down so it hovers over the selected Place_Holder layer then hold 
down Alt (on Windows) or Option (on Mac) and release. The footage should now have 
replaced the place holder. Depending on the size of your footage or image you may want 
to scale it up or down to suite the template (either select the layer and press the �s� key or 
use the twirly beside the layer in the Timeline panel and twirl down the Transform 
parameters to access the scale function). You can check if your scale is correct by going 
back to the composition in which it appears and deciding if it looks right.

Editing Elements:

You may decide that you don�t want certain elements within the template. Simply locate 
the element within the template structure and toggle the Eye icon next to the layer within 
the Timeline panel to switch off the layer. Selecting the icon again will turn the layer 
back on. 

If you want to change the colours or effects applied to any of the layers within the 
template select the layer and either press the F3 key or select the Effect Controls panel 
(usually docked text to the Project panel). The effects can then be altered using the parameters
of that effect.

Rendering and Resizing:

To render the template out select the Main comp. Go to Composition > Add to Render Queue and select the settings you want. You can also crop 
or resize the final output of the video using the Stretch function (note: selecting the �Lock 
Aspect Ration� check box will keep the proportions of the template correct). Another 
possible resizing option is to create a new composition (Composition > New 
Composition) and selecting the format you want. Drag the Main comp into the new 
composition panel and resize the layer to suite your new format (note: this may mean 
either cropping the edges or creating a letterbox to maintain the proportions of the 
template). 

Thank you and enjoy the file.

Hedley


