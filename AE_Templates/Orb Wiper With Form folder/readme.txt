Report created: 	4/2/09	1:29:57 PMProject name: Orb Wiper With Form.aep
Thanks for purchasing this After Effects Template!

This template uses 2 plug-ins from Red Giant Software.

Trapcode Form and Trapcode Particular

Upon opening the project you will see 10 layers. The bottom most layer contains the sound fx layer that syncs with the movement of the text.

The only layer you need to be concerned about is layer 9 "Text Layer". Option or Alt Click on this layer to edit the text.

Double Click the layer to edit the text. Also make sure your text fits between the 2 guides you see. Your text needs to be this big in order for it to look right. So if you only have one word, you'll have to make it a little bigger. If it's longer than the text I provided you'll have to make it smaller. Just make sure it fits between the 2 guides.

Go back to the comp, "Orb Wiper With Form". You will see the results with the text you typed in. That's it!

Now just render out.

Again, thanks for purchasing this after effects template!

..............................................
Cassidy Bisher
President
AE Projects.Biz
http://www.aeprojects.biz

Source files collected: AllCollected comps:  	Orb Wiper With Form	Text Here	Number of collected files:  2Size of collected files:  8.0 MB	Rendering plug-ins:	Advanced 3D	Effects used:  3Effect:  Form	Effect:  Particular	Effect:  Separate XYZ Position	Layer:  Text Layer	External dependencies:  		Font family: 'Times', Font style: 'Regular'