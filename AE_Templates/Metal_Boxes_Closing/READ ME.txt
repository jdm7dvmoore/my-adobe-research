READ ME Metal Boxes:

Instructions:

*Do not edit Aqua Layers.
*Only edit Red Layers.

*Render either "Render 864 x 486" or "Render 1920 x 1080"
 located under the "Comps (EDIT ME)" folder, "Render Comps" folder.

Edit Comps under "Pre Comps (EDIT ME) folder.

 *Change the text for the tag in "Text Tag 1(Edit Me)" Comp.
 *Change the text for the tag in "Text Tag 2(Edit Me)" Comp.
 *Place logo or big text in "Tag Logo(EDIT ME)" comp.  Once you place
	logo in comp turn off text layer.

Change text for Metal boxes:

 *Change the text in"Text 1(Edit Me)" Comp.
 *Change the text in"Text 2(Edit Me)" Comp.
 *Change the text in"Text 3(Edit Me)" Comp.

Render.