Report created: 	5/5/08	12:05:35 PMProject name: Smoke Animation Trailer.aepThanks for purchasing this AE file! (Don't forget to watch the video tutorial on how to change the color from red to whatever you want. The file is included in this download. It's a Quicktime file called "Tutorial.mov"

There are 6 comps that come with this file.

Each comp has a Color Correction Adjustment Layer. It is the first layer (layer 1) 

Click on this layer to see it's effects in the effect controls. You'll see that the Hue/Saturation effect is below the Curves Layer. You can enable this effect by clicking the little box next to "Hue/Saturation" to adjust the color of your comp. When you unhide this effect, you'll see the whole comp turn blue...

Adjust the "Master Hue" to change the color of your Comp to what you desire. I just chose blue.

Let's start with Animation 01.

Basically all you have to do is double click on the layers, (5,6 and 7) typing in the same exact thing. layers 5 and 6 are a glow and shimmer. You can adjust their position if you would like to. But make sure you adjust each one exactly the same. I do this by shift clicking on each layer 5, 6 and 7 so they are all highlighted. Then I move them around.

Animation 02

You'll notice there isn't a photo in the picture, just a placeholder. You'll have to put one in. You can put video in as well. 

Option or alt click on the "Picture Frame 01" layer (layer 5 or 6). This will open up the comp.

Put your photo under the adjustment layer. (Delete my Placeholder and Text) The adjustment layer has color correction that de-saturates the image. I think that black and white photos look really good with the black and red theme here. I'll leave that artistic choice up to you. If you don't want the black and white photos just hide the adjustment layer.

Remember to make sure your photo doesn't bleed over underneath the picture frame. If your photo isn't exactly vertical shape and it's more horizontal in shape, just increase the scale size and crop off the photo with a mask. Use the rectangle tool (short-cut is Q), to draw and shape on the picture to cut off what you don't want to show using a mask. 

Or you can make sure your photo is more vertical in shape before you bring it in to AE. In a program like photoshop or something. Am I telling you stuff you already know? Sorry!

Again make sure with the text you change all the text to what you want on layers 7, 8 and 9 which has the glow and shimmer effect.

Animation 03

This is the same as "Animation 02" Just a different camera move. Again if you need more of these animations just render out and swap in the next photo, then render out again. Use these Animations if you need more photos for your presentations.

Animation 04

Again double click on the text layers to edit the text. Also on layer 04 I am using a light ray that I created in Photoshop. If you need to change the position just click on the layer to make sure it's highlighted and move it around. I have rotation keyframes on this layer only.

Animation 05

Put in your pictures, not only can you option or alt click on the layers you can find the comps for these picture frames in the "Comps" folder in the Project Window.

Bring your photos into comps 3 through 7. Then open up Animation 05 and they will all be there. If you click on things in this window you may accidently click on a cloud of smoke. So that is why clicking on layers may be a better option. Sometimes I lock the layers I don't want to accidently click.

Also for the text layers on layers 13 and 14 I have keyframes on the scale and position. To make some subtle movement and a good ending spot at the end of the last photo. I would change the anchor point for this layer if you want to move it around.

When your done editing all the comps, open up the comp "Final Comp To Render" and render away! All the comps you just worked on are nested in this comp. You can adjust the order of them as well in this comp if you choose to.

The lower 3rd Comp is the last one. Just put in your video over layer 9 and it will look good. Also double click on layers 2 and 3 to edit the text. 

You will notice some slight movement with the smoke layers. This is done with the turbulent displace effect. If you know what you are doing  you can speed this up for a cool effect. 
  
Last step is to open up the Final Comp to Render. This Comp contains all the animations synced with the music and sound FX's. This is the comp you should render out using the render queue. The acoustic guitar I used was a Martin for those of you who know guitars. What does that mean? It sounds good!

Also there is a clip in the footage folder that I am using for a light flare, feel free to use this on transitions if you have any that you are using this project with. The file is called, "Vortex_Bright_Light.mov"

That's all Folks!

Cassidy Bisher
Source files collected: AllCollected comps:  	Smoke Animation	Animation 01	Animation 02	Picture Frame 01	Animation 03	Picture Frame 02	Animation 04	Animation 05	Picture Frame 03	Picture Frame 04	Picture Frame 05	Picture Frame 06	Picture Frame 07	Lower 3rd	Number of collected files:  4Size of collected files:  72.6 MB	Rendering plug-ins:	Advanced 3D	Effects used:  11Effect:  CC Particle World	Effect:  Colorama	Effect:  Curves	Effect:  Drop Shadow	Effect:  Exposure	Effect:  Gaussian Blur	Effect:  Glow	Effect:  Hue/Saturation	Effect:  Lens Flare	Effect:  Motion Tile	Effect:  Turbulent Displace	Layer:  Text Layer	External dependencies:  		Font family: 'Arial Black', Font style: 'Regular'		Font family: 'Arial', Font style: 'Regular'