Report created: 	9/11/08	10:25:11 AMProject name: Particle Fractal Fly-Through.aepThanks for purchasing this file!

With this file you need the 3rd party plugin "Form" from trapcode particular.

To edit the text just double click on the text layers.

Drop in your video or pictures using the comps that are in the folder in your project window called COMPS. Toggle down that folder and you'll see them. They are titled, "Video Border 01 and Video Border 02" Delete my placeholder and put in what ever you want.

You'll want to edit the compositions "Animation 01 through 04." Then once you have edited all of these... open up the "Final Comp To Render" Composition. This composition has all the Comps nested together with the music and sound FX's perfectly synced to the animation.

Don't forget to check the box for audio once you render using the render que! Overall this file is pretty simple to edit. I hope it makes you look fantastic.


Happy animating!

AE Projects
Cassidy Bisher


 Source files collected: AllCollected comps:  	Animation 01	Animation 02	Animation 03	Video Border 01	Video Border 02	Final Comp To Render	Animation 04	Number of collected files:  7	Rendering plug-ins:	Advanced 3D	Effects used:  3Effect:  Hue/Saturation	Effect:  Lens Flare	Effect:  Ramp	Layer:  Text Layer	External dependencies:  		Font family: 'Arial Black', Font style: 'Regular'