Report created: 	1/22/09	4:34:43 PMProject name: The Dark Room.aep

Thank you very much for purchasing "The Dark Room". This project is a particularly favorite of mine. 

First off if you own trapcode particular you will want to work with the trapcode version. It's called, "The Dark Room Trapcode.aep"

If you don't own trapcode particular please use the file called, "The Dark Room.aep"

The first comp you will want to work with is called, "The Dark Room" once you open the project file. In the layer window go all the way to the bottom. You'll see the first block of text to edit. Double click on the text layers in the layer window to edit the text. 

The camera moves to it's 2nd mark. You will see text and a PlaceHolder Graphic. Edit the text. Then option or alt click (In the Layer Window) Video PlaceHolder 01. Or toggle down the "Video PlaceHolders" folder in the project window. You can double click on all the placeholders here and drop in your photo's or video if you would like. Place them all under the "Video Box" layer on layer 1.

The Camera moves to it's 3rd spot. You'll see text here with no placeholder. Double click on the text layers to edit them.

The Camera moves to it's 4th spot. Here you will see another video placeholder. Option or alt click on Video PlaceHolder 02. Delete my PlaceHolder graphic and put in your photo or video. Edit the text.

The Camera moves to it's 5th spot. Here you will see yet another video placeholder. Option or alt click on Video PlaceHolder 03. Delete my PlaceHolder graphic and put in your photo or video under the white video box. Edit the text in the main comp.

Next we have a nested comp. At about frame 25 you will see all the way up at the top of the layer window a nested comp called, "The Dark Room Ending". I built this comp this way so if you wanted to have a comp that you could edit for other things you needed you could use this one. But for this main animation all you have to do is option or alt click on the layer, you will see that it opens up. Now double click on the text in the layer window to edit. Now go back to the main comp, "The Dark Room"

You will see that your text now shows up in the main comp. Move to frame 30.

This is where you will see a nested composition containing a lower 3rd that closes out the animation. Option or Alt Click to edit. Delete My PlaceHolder Graphic. Put your Photo or Video at the very bottom layer of the "Lower 3rd" Comp. Double Click to edit the text.

Go back to the main comp "The Dark Room"

The music and sound fx are sycned to the animation here. You will see them at the bottom. Tip, press the period key on where your number keys are located on the right. You'll here the audio preview. This is the Comp you will want to export and render. 

Hope that helps!

Again thanks for purchasing this after effects template!

Cassidy Bisher


Source files collected to: 	Macintosh HD:After Effects Projects:The Dark Room folderSource files collected: AllCollected comps:  	The Dark Room	Video PlaceHolder 01	Video PlaceHolder 02	Video PlaceHolder 03	The Dark Room Ending	Lower 3rd	Number of collected files:  7Size of collected files:  43.4 MB	Rendering plug-ins:	Advanced 3D	Effects used:  5Effect:  Exposure	Effect:  Glow	Effect:  Motion Tile	Effect:  Particular	Effect:  Separate XYZ Position	Layer:  Text Layer	External dependencies:  		Font family: 'Arial Black', Font style: 'Regular'