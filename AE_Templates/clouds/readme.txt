{\rtf1\ansi\ansicpg1252\cocoartf949\cocoasubrtf330
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
\margl1440\margr1440\vieww9000\viewh7800\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\ql\qnatural\pardirnatural

\f0\fs24 \cf0 Thank you for buying "Clouds 01"\
\
This project features a 3D camera moving around to replaceable video area and then landing on a logo. You can put your own video or stills in each of the areas simply by changing out the content in the "box" comps.\
\
I hope you enjoy using this project and are inspired to greater levels of AE production.\
\
Thanks,\
Steve\
Digital Spatula}