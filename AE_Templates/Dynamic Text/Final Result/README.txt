The final project file 'dynamic text.aep' was created and saved in the Dynamic Text folder on the the Desktop of a MAC.
/Users/[your username]/Desktop/Dynamic Text/Final Result/dynamic text.aep

If the file is in a different location or on a PC the first time you open it you will get an error about the expressions used.
The expressions for the dynamic text link to the textlist.txt file. If AE cannot find the file it will prompt with an error for each occurrence.