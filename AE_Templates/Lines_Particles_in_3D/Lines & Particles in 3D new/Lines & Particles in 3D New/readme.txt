Report created: 	4/24/08	12:06:49 PMProject name: Lines in 3D.aep

Great! Thanks for downloading this file. This file is simple and easy to edit. 

There are 2 version of this file. One has trapcode particular layers and the other has pre-rendered quicktime movies of the particles if you don't own the plug ins. 

Replace the placeholders with your photos or video.

These Comps are nested in the Animation Compositions.

Open up the "Comps" folder in the project window. And you'll see all the comps you will need to place your video or photos in.

Double click on "Video Box 001" and put it underneath the white box. Should look good. You can also do video.

Repeat this process for Video Box 002 and Video Box 003.

For text, just double click on the layer to edit. Also there is a ramp effect on the text layer.. you can modify this if you would like.

This project file is broken down into 7 COMPS. When you are done editing all the Animation comps, you'll need to open up "Final Comp to Render". This has all the comps nested into one timeline. It also has the audio and sound FX's that sync to this animation.

I use the Render Queue. Go to Composition > Make Movie to bring your comps into the render queue.

It's really quite simple. Amazing what things can look like with a few simple filters. 

Happy Rendering!
Cassidy Bisher
Source files collected: AllCollected comps:  	Animation 01	Animation 02	Video Box 001	Animation 03	Video Box 002	Video Box 003	Animation 04	Animation 05	Number of collected files:  1Size of collected files:  171 KB	Rendering plug-ins:	Advanced 3D	Effects used:  11Effect:  Bezier Warp	Effect:  CC Light Rays	Effect:  CC Particle World	Effect:  Drop Shadow	Effect:  Fast Blur	Effect:  Fractal Noise	Effect:  Gaussian Blur	Effect:  Glow	Effect:  Hue/Saturation	Effect:  Lens Flare	Effect:  Ramp	Layer:  Text Layer	External dependencies:  		Font family: 'Helvetica', Font style: 'Bold'