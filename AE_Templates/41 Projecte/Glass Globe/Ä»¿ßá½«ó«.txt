This is an After Effects glass globe that is made solely with solid layers with 
effects (no external files). It requires CC Sphere and also uses Trapcode's Starglow 
(but Starglow is not necessary).

SUPPORT INFORMATION
This file is also on the tutorials section of Creativecow.net - with a step-by-step tutorial. 