#!Z:\adobe\github\opensource\afdko\FDK\Tools\win\Python\AFDKOPython27\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'fonttools==3.9.1','console_scripts','pyftinspect'
__requires__ = 'fonttools==3.9.1'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('fonttools==3.9.1', 'console_scripts', 'pyftinspect')()
    )
